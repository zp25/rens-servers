#!/bin/bash
#
# Apache HTTP Server install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${APACHEDIR}
    SRCDIR_APR=${BASEDIR}/${APRDIR}
    SRCDIR_APRU=${BASEDIR}/${APRUTILDIR}
    SRCDIR_PCRE=${BASEDIR}/${PCREDIR}
fi

if [ ! -d $SRCDIR ] || [ ! -d $SRCDIR_APR ] || [ ! -d $SRCDIR_APRU ] || [ ! -d $SRCDIR_PCRE ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/httpd.conf
# install path
InstallPath=/usr/local/apache2
# config filename
Conf=${InstallPath}/conf/httpd.conf
# data-dir & log-dir
DataPath=/var/www
LogPath=/var/log/apache2/logs


# Install
mv $SRCDIR_APR ${SRCDIR}/srclib/apr
mv $SRCDIR_APRU ${SRCDIR}/srclib/apr-util

cd $SRCDIR_PCRE
make clean
./configure --prefix=/opt/pcre
make && make install

cd $SRCDIR
make clean
./configure --prefix=${InstallPath} --with-included-apr --with-pcre=/opt/pcre
make && make install

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, backup $Conf and mv $ConfBase to $Conf; else pass
if [ -f $ConfBase ]; then
    mv $Conf ${Conf}.backup
    mv $ConfBase $Conf && chmod 644 $Conf
fi

# 2.data-dir & log-dir
if [ ! -d $DataPath ]; then
    mkdir -p $DataPath
fi

if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

chown -R www-data:www-data $DataPath
find $DataPath -type d -exec chmod g=rxs "{}" \;  # rwxr-sr-x
find $DataPath -type f -exec chmod 644 "{}" \;    # rw-r--r--

# 3.easy control
ln -s ${InstallPath}/bin/apachectl /etc/init.d/apache2