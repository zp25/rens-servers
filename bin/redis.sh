#!/bin/bash
#
# Redis install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${REDISDIR}
fi

if [ ! -d $SRCDIR ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# If you modify the port, you need to edit redis config file & init script
Port=$REDISPORT
# custom config file
ConfBase=${BASEDIR}/redis.conf
# config path & config filename
ConfPath=/etc/redis
Conf=${ConfPath}/${Port}.conf
# data-dir & log-dir
DataPath=/var/redis/${Port}
LogPath=/var/log/redis


# Install
id redis 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
    groupadd redis
    useradd -g redis redis
fi

cd $SRCDIR
make clean
make

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, moved to $ConfPath
#   If $ConfBase not exitsts:
#     $Conf has already exists, pass
#     no $Conf file, exit
if [ ! -d $ConfPath ]; then
    mkdir -p $ConfPath
fi

if [ ! -f $ConfBase ]; then
    if [ ! -f $Conf ]; then
        echo 'Error: Can not find redis.conf'
        exit 1
    else
        printf "Config file %s already exists\n" $Conf
    fi
else
    mv $ConfBase $Conf && chmod 644 $Conf
fi

# 2.data-dir & log-dir
if [ ! -d $DataPath ]; then
    mkdir -p $DataPath
fi

if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

# 3.easy control
cp src/redis-server /usr/local/bin/redis-server
cp src/redis-cli /usr/local/bin/redis-cli
cp src/redis-benchmark /usr/local/bin/redis-benchmark
cp src/redis-check-aof /usr/local/bin/redis-check-aof
cp src/redis-check-dump /usr/local/bin/redis-check-dump

cp utils/redis_init_script /etc/init.d/redis_${Port}