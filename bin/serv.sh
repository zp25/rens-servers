#!/bin/bash

# /etc/init.d/apache2 start|stop|restart
# /etc/init.d/mysql start|stop|restart
# nginx -s stop|quit|reload|reopen
# mongod --config /etc/mongod.conf
# php-fpm
# /etc/init.d/redis_6379 start|stop

LIST=('httpd' 'mysql' 'nginx' 'mongo' 'php-fpm' 'redis')
REDISPORT=6379

getpid() {
    prc=$1
    if [ `pgrep -c $prc` -gt 0 ]; then
        pid=`pgrep -u 0 $prc`
        echo $pid
    else
        echo 0
    fi
}

start_h() {
    serv=${LIST[$1]}
    pid=$(getpid $serv)
    if [ $pid -ne 0 ]; then
        printf "%s (pid %s) is running\n" $serv $pid
        return 0
    fi

    case $serv in
        'httpd')
            /etc/init.d/apache2 start 1>/dev/null 2>&1 ;;
        'mysql')
            /etc/init.d/mysql start 1>/dev/null 2>&1 ;;
        'nginx')
            nginx 1>/dev/null 2>&1 ;;
        'mongo')
            mongod --config /etc/mongod.conf 1>/dev/null 2>&1 ;;
        'php-fpm')
            php-fpm 1>/dev/null 2>&1 ;;
        'redis')
            /etc/init.d/redis_${REDISPORT} start 1>/dev/null 2>&1 ;;
    esac

    return $?
}

restart_h() {
    serv=${LIST[$1]}
    pid=$(getpid $serv)
    if [ $pid -eq 0 ]; then
        printf "%s is not running\n" $serv
        return 0
    fi

    case $serv in
        'httpd')
            /etc/init.d/apache2 restart 1>/dev/null 2>&1 ;;
        'mysql')
            /etc/init.d/mysql restart 1>/dev/null 2>&1 ;;
        'nginx')
            nginx -s reload 1>/dev/null 2>&1 ;;
        'mongo'|'php-fpm'|'redis')
            stop_h $1 && start_h $1
    esac

    return $?
}

stop_h() {
    serv=${LIST[$1]}
    pid=$(getpid $serv)
    if [ $pid -eq 0 ]; then
        printf "%s is not running\n" $serv
        return 0
    fi

    case $serv in
        'httpd')
            /etc/init.d/apache2 stop 1>/dev/null 2>&1 ;;
        'mysql')
            /etc/init.d/mysql stop 1>/dev/null 2>&1 ;;
        'nginx')
            nginx -s quit 1>/dev/null 2>&1 ;;
        'mongo'|'php-fpm')
            kill $pid 1>/dev/null 2>&1 ;;
        'redis')
            /etc/init.d/redis_${REDISPORT} stop 1>/dev/null 2>&1 ;;
    esac

    return $?
}


main() {
    # prepare
    if [ $EUID -ne 0 ]; then
        echo 'ERROR: You need to be root to run this script'
        exit 1
    fi

    # select services
    printf "Services:\n"

    count=${#LIST[@]}
    for((i=0; i<$count; i++))
    do
        printf "    %-10s%d\n" ${LIST[$i]} $i
    done

    printf "\nExample: %s>0 2 3\n" $1
    read -p "$1>" -a servs

    if [ ${#servs[@]} -eq 0 ] || [ ${servs[0]} = 'all' ]; then
        for((i=0; i<$count; i++))
        do
            servs[$i]=$i
        done
    fi

    for sid in ${servs[@]}
    do
        # !
        if [[ $sid =~ ^[0-5]$ ]]; then
            case $1 in
                'start')
                    start_h $sid ;;
                'restart')
                    restart_h $sid ;;
                'stop')
                    stop_h $sid ;;
            esac

            if [ $? -eq 0 ]; then
                printf "%-10s done\n" ${LIST[$sid]}
            else
                printf "%-10s failed\n" ${LIST[$sid]}
            fi
        else
            printf "Invalid arg: %s\n" $sid
        fi
    done
}

status() {
    for serv in ${LIST[@]}
    do
        pid=$(getpid $serv)
        if [ $pid -ne 0 ]; then
            printf "%-10s%-5s(pid %d)\n" $serv 'on' $pid
        else
            printf "%-10s%s\n" $serv 'off'
        fi
    done
}

opt=$1
case $opt in
    'status')
        status ;;
    'start'|'restart'|'stop')
        main $opt ;;
    'help'|'')
        echo 'Usage: serv.sh { status | start | restart | stop | help }' ;;
    *)
        printf "serv.sh : invalid option -- '%s'\n" $opt
        printf "Try 'serv.sh help' for more information.\n"
        exit 1 ;;
esac