#!/bin/bash

# Packaged dependencies
#
# mysql: libaio1
#   enable userspace to use Linux kernel asynchronous I/O sys calls
# php: libxml2-dev m4(autoconf) autoconf(phpize)
# php-fpm: libtool libjpeg62-dev(GD)
# nginx: openssl libssl-dev
# node: python-software-properties python nodejs
# add-apt-repository ppa:chris-lea/node.js
apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -yq build-essential curl git \
        libaio1 \
        libxml2-dev m4 autoconf \
        libtool libjpeg62-dev \
        openssl libssl-dev

# python-software-properties python nodejs