#!/bin/bash
#
# Mongodb install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${MONGODIR}
    SRCDIR_DRIVER=${BASEDIR}/${MONGODRIVERDIR}
fi

if [ ! -d $SRCDIR ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/mongod.conf
# install path
InstallPath=/usr/local/mongodb
# config filename
Conf=/etc/mongod.conf
# data-dir & log-dir
DataPath=/var/mongodb/data
LogPath=/var/log/mongodb


# Install
mv $SRCDIR $InstallPath
cd $InstallPath

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, moved to $ConfPath
#   If $ConfBase not exitsts:
#     $Conf has already exists, pass
#     no $Conf file, exit
if [ ! -f $ConfBase ]; then
    if [ ! -f $Conf ]; then
        echo 'Error: Can not find mongodb.conf'
        exit 1
    else
        printf "Config file %s already exists\n" $Conf
    fi
else
    mv $ConfBase $Conf && chmod 644 $Conf
fi

# 2.data-dir & log-dir
if [ ! -d $DataPath ]; then
    mkdir -p $DataPath
fi

if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

# 3.easy control
# use $PATH


# install driver
if [ $MONGODRIVER = 1 ] || [ $MONGODRIVER = true ]; then
    if [ ! -d $SRCDIR_DRIVER ]; then
        echo 'Error: Can not find mongo-php-driver'
        exit 1
    fi

    PhpPath=/usr/local/php5-cgi

    cd $SRCDIR_DRIVER
    ${PhpPath}/bin/phpize --clean
    ${PhpPath}/bin/phpize
    ./configure --with-php-config=${PhpPath}/bin/php-config
    make
    cp modules/mongo.so ${PhpPath}/lib/php/extensions/mongo.so
fi