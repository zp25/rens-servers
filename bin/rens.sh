#!/bin/bash
#
# rens enterpoint

cd ../
BASEDIR=$PWD
BASEENV=${BASEDIR}/bin/default.sh

declare -a LIST_SERV
declare -a LIST_CONF
declare -a LIST_UBIN

LIST=("lamp" "lnmp" "mysql" "mongodb" "redis")

MONGODRIVER=false

# make sure you have default.sh in $BASEDIR/bin
if [ ! -f $BASEENV ]; then
    printf "Error: Can not find %s\n" $BASEENV
    exit 1
else
    source $BASEENV
    FILEDIR=${BASEDIR}/files
    CONFDIR=${BASEDIR}/config
    UBINDIR=${BASEDIR}/bin
fi


getApache() {
    if [ ! -f $APACHEARC ]; then
        echo 'Downloading Apache HTTP Server'
        # Other mirrors: http://mirrors.cnnic.cn/apache/
        curl -O# http://www.us.apache.org/dist//httpd/$APACHEARC
    fi

    # requires at least 50MB free disk space and apr, apr-util, pcre.
    if [ ! -f $APRARC ]; then
        echo 'Downloading APR'
        curl -O# http://www.us.apache.org/dist//apr/$APRARC
    fi

    if [ ! -f $APRUTILARC ]; then
        echo 'Downloading APR-util'
        curl -O# http://www.us.apache.org/dist//apr/$APRUTILARC
    fi

    if [ ! -f $PCREARC ]; then
        echo 'Downloading PCRE'
        # or ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/$PCREARC
        curl -O# http://jaist.dl.sourceforge.net/project/pcre/pcre/$PCRE/$PCREARC
    fi
}

getMysql() {
    if [ ! -f $MYSQLARC ]; then
        echo 'Downloading Mysql'
        # /Downloads/MySQL-5.6/mysql-5.6.17-linux-glibc2.5-x86_64.tar.gz
        curl -O# http://cdn.mysql.com/Downloads/$MYSQLBASE/$MYSQLARC
    fi
}

getPhpmod() {
    if [ ! -f $PHPARC ]; then
        echo 'Downloading PHP'
        curl -O# http://jp1.php.net/distributions/$PHPARC
    fi

    # xdebug
    if [ ! -f $XDEBUGARC ]; then
        echo 'Downloading xdebug'
        curl -O# http://xdebug.org/files/$XDEBUGARC
    fi
}

getNginx() {
    if [ ! -f $NGINXARC ]; then
        echo 'Downloading Nginx'
        curl -O# http://nginx.org/download/$NGINXARC
    fi

    # Nginx requires zlib and pcre
    if [ ! -f $ZLIBARC ]; then
        echo 'Downloading zlib'
        curl -O# http://zlib.net/$ZLIBARC
    fi

    if [ ! -f $PCREARC ]; then
        echo 'Downloading PCRE'
        # or ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/$PCREARC
        curl -O# http://jaist.dl.sourceforge.net/project/pcre/pcre/$PCRE/$PCREARC
    fi
}

getPhpcgi() {
    if [ ! -f $PHPARC ]; then
        echo 'Downloading PHP'
        curl -O# http://jp1.php.net/distributions/$PHPARC
    fi

    # freetype libpng zlib xdebug
    if [ ! -f $FREETYPEARC ]; then
        echo 'Downloading freetype'
        curl -O# http://ftp.twaren.net/Unix/NonGNU//freetype/$FREETYPEARC
    fi

    if [ ! -f $LIBPNGARC ]; then
        echo 'Downloading libpng'
        curl -O# http://jaist.dl.sourceforge.net/project/libpng/$LIBPNGBASE/$LIBPNG/$LIBPNGARC
    fi

    if [ ! -f $ZLIBARC ]; then
        echo 'Downloading zlib'
        curl -O# http://zlib.net/$ZLIBARC
    fi

    if [ ! -f $XDEBUGARC ]; then
        echo 'Downloading xdebug'
        curl -O# http://xdebug.org/files/$XDEBUGARC
    fi
}

getMongo() {
    if [ ! -f $MONGOARC ]; then
        echo 'Downloading Mongodb'
        curl -O# http://fastdl.mongodb.org/linux/$MONGOARC
    fi

    if [ ! -d $MONGODRIVERDIR ]; then
        echo 'Downloading mongo-php-driver'
        mkdir -p $MONGODRIVERDIR
        git clone https://github.com/mongodb/mongo-php-driver ${FILEDIR}/${MONGODRIVERDIR}
    fi
}

getRedis() {
    if [ ! -f $REDISARC ]; then
        echo 'Downloading Redis'
        curl -O# http://download.redis.io/releases/$REDISARC
    fi
}

md5check() {
    if [ ! -f hash.md5 ]; then
        echo 'File hash.md5 not found.'
    else
        md5sum -c hash.md5
    fi
}

main() {
    # prepare
    if [ $EUID -ne 0 ]; then
        printf "ERROR: You need to be root to run this script\n"
        exit 1
    fi

    if [ ! -d $FILEDIR ]; then
        mkdir $FILEDIR
    fi

    # download archives
    cd $FILEDIR
    otp_s=$1
    case $otp_s in
        'lamp')
            LIST_SERV+=($APACHEARC $APRARC $APRUTILARC $PCREARC $MYSQLARC $PHPARC $XDEBUGARC)
            LIST_CONF+=('httpd.conf' 'my.cnf' 'php.ini' 'phpinfo.php')
            LIST_UBIN+=('apache.sh' 'mysql.sh' 'php_mod.sh')

            getApache
            getMysql
            getPhpmod ;;
        'lnmp')
            LIST_SERV+=($NGINXARC $ZLIBARC $PCREARC $PHPARC $FREETYPEARC $LIBPNGARC $XDEBUGARC $MONGOARC $MONGODRIVERDIR)
            LIST_CONF+=('nginx.conf' 'php-cgi' 'php-fpm.conf' 'mongod.conf' 'phpinfo.php')
            LIST_UBIN+=('nginx.sh' 'php_cgi.sh' 'mongo.sh')

            MONGODRIVER=true

            getNginx
            getPhpcgi
            getMongo ;;
        'mysql')
            LIST_SERV+=($MYSQLARC)
            LIST_CONF+=('my.cnf')
            LIST_UBIN+=('mysql.sh')

            getMysql;;
        'mongodb')
            LIST_SERV+=($MONGOARC)
            LIST_CONF+=('mongod.conf')
            LIST_UBIN+=('mongo.sh')

            MONGODRIVER=false

            getMongo ;;
        'redis')
            LIST_SERV+=($REDISARC)
            LIST_CONF+=('redis.conf')
            LIST_UBIN+=('redis.sh')

            getRedis ;;
        'help'|'')
            helpInstall
            exit 0 ;;
        *)
            printf "rens.sh install: invalid option -- '%s'\n" $otp_s
            printf "Try 'rens.sh install help' for more information.\n"
            exit 1 ;;
    esac

    # build
    uselocal

    echo 'Done'
}

uselocal() {
    RENSDIR=${BASEDIR}/rens
    if [ ! -d $RENSDIR ]; then
        mkdir $RENSDIR
    fi

    cd $RENSDIR
    export MONGODRIVER

    cp ${UBINDIR}/default.sh ${RENSDIR}/default.sh
    cp ${UBINDIR}/serv.sh /usr/local/bin/serv.sh
    chmod 755 /usr/local/bin/serv.sh

    printf "# add archives\n"
    for serv in ${LIST_SERV[@]}
    do
        printf "Extracting %s\n" $serv
        if [ -d ${FILEDIR}/${serv} ]; then
            cp -r ${FILEDIR}/${serv} ${RENSDIR}/${serv}
        else
            tar -zxf ${FILEDIR}/${serv} -C $RENSDIR
        fi
    done

    printf "# add config files\n"
    for conf in ${LIST_CONF[@]}
    do
        cp -r ${CONFDIR}/${conf} ${RENSDIR}/${conf}
        chmod 644 ${RENSDIR}/${conf}
    done

    printf "# add scripts & run\n"
    for ubin in ${LIST_UBIN[@]}
    do
        cp ${UBINDIR}/${ubin} ${RENSDIR}/${ubin}
        chmod 700 ${RENSDIR}/${ubin}
        ./${ubin}
    done

    cd $BASEDIR
    rm -rf $RENSDIR
}

helpInstall() {
    cat <<helpInstall
Usage: rens.sh install [Service]
Services:
    lamp      Apache HTTP Server + Mysql + PHP
    lnmp      Nginx + Mongodb + PHP
    mysql     Mysql Only
    mongodb   Mongodb Only
    redis     Redis Only
Example: rens.sh install lamp
helpInstall
}

opt=$1
case $opt in
    'install')
        main $2 ;;
    'help'|'')
        printf "Usage: rens.sh { install | help }\n" ;;
    *)
        printf "rens.sh : invalid option -- '%s'\n" $opt
        printf "Try 'rens.sh help' for more information.\n"
        exit 1 ;;
esac