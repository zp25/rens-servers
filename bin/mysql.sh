#!/bin/bash
#
# Mysql install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${MYSQLDIR}
fi

if [ ! -d $SRCDIR ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/my.cnf
# install path
InstallPath=/usr/local/mysql
# config filename
Conf=${InstallPath}/my.cnf
# data-dir & log-dir
DataPath=${InstallPath}/data
LogPath=/var/log/mysql


# Install
mv $SRCDIR $InstallPath
cd $InstallPath

id mysql 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
    groupadd -r mysql
    useradd -r -g mysql mysql
fi

# --datadir: data-dir must already exist
# if [ ! -d $DataPath ]; then
#     mkdir -p $DataPath
# fi

chown -R mysql:mysql .

scripts/mysql_install_db --user=mysql --basedir=$InstallPath --datadir=$DataPath \
    --random-passwords

chown -R root:root .
chown -R mysql:mysql data
# chown -R mysql:mysql $DataPath

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, backup $Conf and mv $ConfBase to $Conf; else pass
if [ -f $ConfBase ]; then
    mv $Conf ${Conf}.backup
    mv $ConfBase $Conf && chmod 644 $Conf
fi

# 2.log-dir
if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

# 3.easy control
# ln -s ${InstallPath}/support-files/mysql.server /etc/init.d/mysql
cp ${InstallPath}/support-files/mysql.server /etc/init.d/mysql