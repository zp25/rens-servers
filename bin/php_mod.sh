#!/bin/bash
#
# PHP apache-mod (mysql) install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${PHPDIR}
    SRCDIR_XDEBUG=${BASEDIR}/${XDEBUGDIR}
fi

if [ ! -d $SRCDIR ] || [ ! -d $SRCDIR_XDEBUG ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/php.ini
# install path
InstallPath=/usr/local/php5
# config path & config filename
ConfPath=/etc
Conf=${ConfPath}/php.ini


# Install
cd $SRCDIR
make clean
./configure --prefix=$InstallPath \
            --with-config-file-path=$ConfPath \
            --with-apxs2=/usr/local/apache2/bin/apxs \
            --with-mysqli=/usr/local/mysql/bin/mysql_config \
            --with-pdo-mysql
make && make install

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, moved to $ConfPath
#   If $ConfBase not exitsts:
#     $Conf has already exists, pass
#     no $Conf file, exit
if [ ! -f $ConfBase ]; then
    if [ ! -f $Conf ]; then
        echo 'Error: Can not find php.ini'
        exit 1
    else
        printf "Config file %s already exists\n" $Conf
    fi
else
    mv $ConfBase $Conf && chmod 644 $Conf
fi


# install xdebug
cd $SRCDIR_XDEBUG
${InstallPath}/bin/phpize --clean
${InstallPath}/bin/phpize
./configure --with-php-config=${InstallPath}/bin/php-config
make
cp modules/xdebug.so ${InstallPath}/lib/php/extensions/xdebug.so

# phpinfo
DataPath=/var/www
PhpInfoBase=${BASEDIR}/phpinfo.php
PhpInfo=${DataPath}/phpinfo.php

if [ -d $DataPath ] && [ ! -f $PhpInfo ]; then
    mv $PhpInfoBase $PhpInfo && chmod 644 $PhpInfo
fi