#!/bin/bash
#
# Nginx install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${NGINXDIR}
    SRCDIR_ZLIB=${BASEDIR}/${ZLIBDIR}
    SRCDIR_PCRE=${BASEDIR}/${PCREDIR}
fi

if [ ! -d $SRCDIR ] || [ ! -d $SRCDIR_ZLIB ] || [ ! -d $SRCDIR_PCRE ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/nginx.conf
# install path
InstallPath=/usr/local/nginx
# config filename
Conf=${InstallPath}/conf/nginx.conf
# data-dir & log-dir
DataPath=/var/html
LogPath=/var/log/nginx/logs


# Install
cd $SRCDIR_PCRE
make clean

cd $SRCDIR
./configure --prefix=$InstallPath \
            --error-log-path=${LogPath}/error.log \
            --http-log-path=${LogPath}/access.log \
            --user=www-data --group=www-data \
            --with-pcre=../${PCREDIR} --with-pcre-jit \
            --with-zlib=../${ZLIBDIR} \
            --with-http_ssl_module
make && make install


# Prepare to launch
#
# 1.config
#   If $ConfBase exists, mv $ConfBase to $Conf; else pass
if [ -f $ConfBase ]; then
    mv $ConfBase $Conf && chmod 644 $Conf
fi

# 2.data-dir & log-dir
if [ ! -d $DataPath ]; then
    mkdir -p $DataPath
fi

if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

chown -R www-data:www-data $DataPath
find $DataPath -type d -exec chmod g=rxs "{}" \;  # rwxr-sr-x
find $DataPath -type f -exec chmod 644 "{}" \;    # rw-r--r--

# 3.easy control
# use $PATH