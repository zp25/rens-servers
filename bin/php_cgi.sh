#!/bin/bash
#
# PHP cgi (nginx & mongodb) install script

BASEDIR=$PWD
BASEENV=${BASEDIR}/default.sh

# make sure you have default.sh & source code in $BASEDIR
if [ ! -f $BASEENV ]; then
    echo 'Error: Can not find default.sh'
    exit 1
else
    source $BASEENV
    SRCDIR=${BASEDIR}/${PHPDIR}
    SRCDIR_PNG=${BASEDIR}/${LIBPNGDIR}
    SRCDIR_ZLIB=${BASEDIR}/${ZLIBDIR}
    SRCDIR_FREETYPE=${BASEDIR}/${FREETYPEDIR}
    SRCDIR_XDEBUG=${BASEDIR}/${XDEBUGDIR}
fi

if [ ! -d $SRCDIR ] || [ ! -d $SRCDIR_PNG ] || [ ! -d $SRCDIR_ZLIB ] \
     || [ ! -d $SRCDIR_FREETYPE ] || [ ! -d $SRCDIR_XDEBUG ]; then
    echo 'Error: Can not find source code'
    exit 1
fi

# custom config file
ConfBase=${BASEDIR}/php-cgi/php.ini
FpmConfBase=${BASEDIR}/php-fpm.conf
# install path
InstallPath=/usr/local/php5-cgi
# config path & config filename
ConfPath=/etc/php-cgi
Conf=${ConfPath}/php.ini
# fpm config path & fpm config filename
FpmConfPath=${InstallPath}/etc
FpmConf=${FpmConfPath}/php-fpm.conf
# log-dir
LogPath=/var/log/php-fpm


# Install
cd $SRCDIR_PNG
make clean
./configure
make && make install

cd $SRCDIR_ZLIB
make clean
./configure
make && make install

cd $SRCDIR_FREETYPE
make clean
./configure
make && make install

cd $SRCDIR
make clean
./configure --prefix=$InstallPath \
            --with-config-file-path=/etc/php-cgi \
            --enable-fpm \
            --with-fpm-user=www-data --with-fpm-group=www-data \
            --with-gd --with-jpeg-dir=/usr/local/lib --with-png-dir=/usr/local/lib \
            --with-zlib-dir=/usr/local/lib \
            --with-freetype-dir=/usr/local/lib --enable-exif \
            --enable-opcache
make && make install

# Prepare to launch
#
# 1.config
#   If $ConfBase exists, moved to $ConfPath
#   If $ConfBase not exitsts:
#     $Conf has already exists, pass
#     no $Conf file, exit
if [ ! -d $ConfPath ]; then
    mkdir -p $ConfPath
fi

if [ ! -f $ConfBase ]; then
    if [ ! -f $Conf ]; then
        echo 'Error: Can not find php-cgi/php.ini'
        exit 1
    else
        printf "Config file %s already exists\n" $Conf
    fi
else
    mv $ConfBase $Conf && chmod 644 $Conf
fi

if [ -f $FpmConfBase ]; then
    mv $FpmConfBase $FpmConf && chmod 644 $FpmConf
else
    cp ${FpmConf}.default $FpmConf && chmod 644 $FpmConf
fi

# 2.log-dir: edit php-fpm.conf, set error_log
if [ ! -d $LogPath ]; then
    mkdir -p $LogPath
fi

# 3.easy control
# use $PATH


# install xdebug
cd $SRCDIR_XDEBUG
${InstallPath}/bin/phpize --clean
${InstallPath}/bin/phpize
./configure --with-php-config=${InstallPath}/bin/php-config
make
cp modules/xdebug.so ${InstallPath}/lib/php/extensions/xdebug.so

# phpinfo
DataPath=/var/html
PhpInfoBase=${BASEDIR}/phpinfo.php
PhpInfo=${DataPath}/phpinfo.php

if [ -d $DataPath ] && [ ! -f $PhpInfo ]; then
    mv $PhpInfoBase $PhpInfo && chmod 644 $PhpInfo
fi