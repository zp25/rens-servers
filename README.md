# rens-servers
熟悉和整理编译安装过程，尝试bash

版本等配置bin/default.sh

运行bin/pre.sh安装依赖包

运行bin/rens.sh

    # 依照需要添加，如lamp
    export PATH=$PATH:/usr/local/mysql/bin:/usr/local/php5/bin
    alias sudo='sudo env PATH=$PATH'

## mysql
使用--random-passwords，结束后需修改密码，步骤

+ ${HOME}/.mysql_secret获取随机生成的密码
+ 开启mysql服务器
+ 运行sudo mysql_secure_installation
+ 使用随机生成的密码跟随提示进行操作

## 安装完成
可使用bin/serv.sh选择开启服务

localhost/phpinfo.php

localhost/html/phpinfo.php

查看apache nginx和php是否安装成功，配置见nginx.conf

# 参看
+ [Compiling and Installing - Apache HTTP Server](http://httpd.apache.org/docs/current/install.html "Compiling and Installing - Apache HTTP Server")
+ [Installing MySQL on Unix/Linux Using Generic Binaries](http://dev.mysql.com/doc/refman/5.6/en/binary-installation.html "Installing MySQL on Unix/Linux Using Generic Binaries")
+ [PHP: Installation and Configuration](http://php.net/manual/en/install.php "PHP: Installation and Configuration")
+ [Building nginx from Sources](http://nginx.org/en/docs/configure.html "Building nginx from Sources")
+ [Install MongoDB on Linux Systems](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-linux/ "Install MongoDB on Linux Systems")
+ [Redis Quick Start](http://redis.io/topics/quickstart "Redis Quick Start")
